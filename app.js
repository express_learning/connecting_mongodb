const express=require("express")
const mongoose = require("mongoose");

const app=express();

//connection URL
mongoose.connect("mongodb://127.0.0.1:27017/fruitsDB");

//fruit schema ready with validations
const fruitSchema = new mongoose.Schema({
    name: {
        type: String,
        required:[true, "Why no name"]
    },
    rating: {
        type:Number,
        min:1,
        max:10
    },
    review: String
});
 
//Creating Model for fruitSchema
const Fruit = mongoose.model("fruit",fruitSchema);

//Creating person Schema and establish connection with fruitSchema
const personSchema=new mongoose.Schema({
      name:String,
      age: Number,
      favourateFruit:fruitSchema
});

const Person= mongoose.model("Person",personSchema);

const John= new Person({
    name:"John",
    age:37
});
//John.save(); 
const pineapple = new Fruit({
    name:"Pineapple",
    rating:7,
    review:"Great Fruit"
});

//pineapple.save();

const Amy=new Person({
    name:"Amy",
    age:12,
    favourateFruit:pineapple
});

//Amy.save();

//Inserting data in Db 
const mango = new Fruit ({
    name:"Mango",
    rating:7,
    review:"King of fruits."
});

const kiwi = new Fruit ({ 
    name:"Kiwi",
    rating: 9,
    review:"Queen of fruits."
});

const banana = new Fruit ({
    name:"Banana",
    rating: 10,
    review:"Awsome health benifit."
});

 //  mango.save();
// Fruit.insertMany([mango,kiwi,banana]);

// Reading data from database
Fruit.find({}).exec().then(function(docs){
    docs.forEach(function(d){
        console.log(d.name);
    })
    //mongoose.connection.close()
}).catch(function(err){
    if(err){
        console.log(err);
    }
});

//Updating data from fruits DB
Person.updateOne({name:"John"},{favourateFruit: mango}).exec();

//Deleting document from collection
//Fruit.deleteOne({name:"Peach"}).exec();



app.get("/",()=>{
 console.log("fruits saved");
})

app.listen(3000,()=>{
    console.log("Server started at 3000")
})